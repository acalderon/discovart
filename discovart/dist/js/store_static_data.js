/*
* Script for store location info (csv) file
*/

function StoreLocatorDataSource() {
  $.extend(this, new storeLocator.StaticDataFeed);

  var that = this;
  $.get('data/artist_stores.csv', function(data) {
    that.setStores(that.parse_(data));
  }, "text");
}

//Set filter features for artwork options: Oils, Sculpture, Digital, Glass, Photography, Drawing

StoreLocatorDataSource.prototype.FEATURES_ = new storeLocator.FeatureSet(
  new storeLocator.Feature('Oils-YES', 'Oils'),
  new storeLocator.Feature('Sculpture-YES', 'Sculpture'),
  new storeLocator.Feature('Digital-YES', 'Digital'),
  new storeLocator.Feature('Glass-YES', 'Glass'),
  new storeLocator.Feature('Photography-YES', 'Photography'),
  new storeLocator.Feature('Drawing-YES', 'Drawing')
);


StoreLocatorDataSource.prototype.getFeatures = function() {
  return this.FEATURES_;
};


StoreLocatorDataSource.prototype.parse_ = function(csv) {
  var stores = [];
  var rows = csv.toString().split('\n');
  var headings = this.parseRow_(rows[0]);

  for (var i = 1, row; row = rows[i]; i++) {
    row = this.toObject_(headings, this.parseRow_(row));
    var features = new storeLocator.FeatureSet;
    features.add(this.FEATURES_.getById('Oils-' + row.Oils));
    features.add(this.FEATURES_.getById('Sculpture-' + row.Sculpture));
	features.add(this.FEATURES_.getById('Digital-' + row.Digital));
    features.add(this.FEATURES_.getById('Glass-' + row.Glass));
    features.add(this.FEATURES_.getById('Photography-' + row.Photography));
    features.add(this.FEATURES_.getById('Drawing-' + row.Drawing));

    var position = new google.maps.LatLng(row.Ycoord, row.Xcoord);

    var shop = this.join_([row.Shopping_center], ', ');
	var city_state = this.join_([row.City, row.State],', ');
    var city_state_zip = this.join_([city_state, row.Postcode], '  ');

    var store = new storeLocator.Store(row.uuid, position, features, {
      title: row.Store_name,
      address: this.join_([shop, row.Street_add, city_state_zip, row.Hrs_of_bus], '<br>'),
    });
    stores.push(store);
  }
  return stores;
};


StoreLocatorDataSource.prototype.join_ = function(arr, sep) {
  var parts = [];
  for (var i = 0, ii = arr.length; i < ii; i++) {
    arr[i] && parts.push(arr[i]);
  }
  return parts.join(sep);
};


StoreLocatorDataSource.prototype.parseRow_ = function(row) {
  // Strip leading quote.
  if (row.charAt(0) == '"') {
    row = row.substring(1);
  }
  // Strip trailing quote. There seems to be a character between the last quote
  // and the line ending, hence 2 instead of 1.
  if (row.charAt(row.length - 1) == '"') {
    row = row.substring(0, row.length - 1);
  }

  row = row.split('","');

  return row;
};


StoreLocatorDataSource.prototype.toObject_ = function(headings, row) {
  var result = {};
  for (var i = 0, ii = row.length; i < ii; i++) {
    result[headings[i]] = row[i];
  }
  return result;
};