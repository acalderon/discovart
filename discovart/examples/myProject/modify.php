<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>DiscovART</title>

    <!-- Bootstrap core CSS -->
    <link href="../../dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../dist/css/jumbotron.css" rel="stylesheet">
	<link href="../../dist/css/storelocator.css" rel="stylesheet">
	<link href="../../dist/css/range.css" rel="stylesheet">
	<link href="../../dist/css/carousel.css" rel="stylesheet">
	<link href="../../dist/css/mymain.css" rel="stylesheet">
	<link href="../../dist/css/customcolorpicker.css" rel="stylesheet">
	<link href="../../dist/css/progressSteps.css" rel="stylesheet">
	<link href="../../dist/css/newMain.css" rel="stylesheet">


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
	  
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">DivscovART</a>
        </div>
        <div class="navbar-collapse collapse">
			<ul class="nav navbar-nav pull-right">
			  <li><a href="index.html">Home</a></li>
			  <li><a href="artwork.php">Art Work</a></li>
			  <li><a href="locate.html">Locate</a></li>
			  <li class="active"><a href="create.php">Create</a></li>
			  <li><a href="about.html">About</a></li>
			</ul>
        </div><!--/.navbar-collapse -->
      </div>
    </div>
	
    <div class="container">
    <h1>Create Your Own Fun Pic!</h1>
  
 <div class="wizard">
    <a href="create.php"><span class="badge">1</span>Start</a>
    <a href="upload_photo.php"><span class="badge">2</span>Upload</a>
    <a href="modify.php" class="current"><span class="badge badge-inverse">3</span>Modify</a>
    <a id="captionNav" href="#"><span id="capSpan" class="badge">4</span>Caption</a> 
</div> 
<div id="content">
    <div id="steps">
      <h1>Step 3: Reshape and drag your image into position</h1>
    </div>
    <div id="container"></div>
    <form id="textForm" action="modify.php">
      <fieldset>
        <legend>Add Text</legend>
        <p>
          <label for="textBox">Text:</label>
          <input id="textBox" placeholder="Enter a caption" value="" />
        </p>
        <p>
          <label for="textFont">Text Font:</label>
          <select id="textFont">
            <option value="serif">serif</option>
            <option value="sans-serif">sans-serif</option>
            <option value="cursive">cursive</option>
            <option value="fantasy">fantasy</option>
            <option value="monospace">monospace</option>
            <option value="Impact">Impact</option>
          </select>
        </p>
        <p>
          <label for="textSize">Text Size:</label>
          <input type="range" id="textSize" min="0" max="200" value="50"/>
        </p>
      </fieldset>
      <fieldset>
        <legend>Text Color</legend>
        <p>
          <label for="fillType">Fill Type:</label>
          <select id="fillType">
            <option value="colorFill">Solid Color</option>
            <option value="linearGradient">Linear Gradient</option>
            <option value="radialGradient">Radial Gradient</option>
          </select>
        </p>
        <p>
          <label for="textFillColor">Text Color:</label>
          <input class="color" id="textFillColor" value="FF0000"/>
        </p>
        <p>
          <label for="textFillColor2">Gradient Color:</label>
          <input class="color" id="textFillColor2" value ="000000"/>
        </p>
      </fieldset>
    </form>
      
</div>
<div>
<p class="text-center"><button style="width:55%" class="btn btn-primary btn-md" id="saveImage">Save as image</button></p>
</div>	

      <footer class="footerRed">
	  <div style="margin: auto;">
        <p class="text-center">&copy; DiscovART 2013</p>
		</div>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="../../dist/js/modernizr-1.6.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
	<script src="../../dist/js/kinetic-v4.40.js"></script>
	<script src="../../dist/js/myphoto_canvas_custom.js"></script>
	<script src="../../dist/js/binaryajax.js"></script>
	<script src="../../dist/js/imageinfo.js"></script>
	<script src="../../dist/js/html5slider.js"></script>
	<script src="jscolor/jscolor.js"></script>
	
	<script type="text/javascript">
      window.onload = function() {
		var theBg = localStorage.getItem("theChoice");
        var theFile = localStorage.getItem('uploadedFile');
		var captionFlag = 'false';
		theFile = "uploads/" + theFile;
		theBg = "images/" + theBg;
		var sources = {
          myBg: theBg,
          myImage: theFile
        };

		ImageInfo.loadInfo(theFile, getDimensions);
        loadImages(sources, initStage);

		function getDimensions() {
			var theWidth = ImageInfo.getField(theFile, "width")/2;
			var theHeight = ImageInfo.getField(theFile, "height")/2;
			localStorage.setItem("imageWidth",theWidth);
			localStorage.setItem("imageHeight",theHeight);
			}
		
		$("#captionNav").click(function(e) {
			e.preventDefault();
			$("#textForm").css('display','block');
			$theInfo = "<h1>Step 4: Add a caption to your photo card and drag it into place</h1>";
			$("#steps").html($theInfo);
			//my additions

			//Remove and Change: <a> class and <span> class
			$('a.current').removeClass('current'); //a
			$('span.badge.badge-inverse').addClass('badge').removeClass('badge badge-inverse'); //span
			
			//Assign new to: <a> and <span>
			$('a#captionNav').addClass('current'); //a
			$('span#capSpan').addClass('badge badge-inverse').removeClass('badge'); //span
			
		});

		
		 $('input').keydown( function(e) {
				var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
				if(key == 13) {
					e.preventDefault();
					var inputs = $(this).closest('form').find(':input:visible');
					inputs.eq( inputs.index(this)+ 1 ).focus();
				}
			});		
      };
</script>
  </body>
</html>
