<?php
include 'lib.php';
/*
Server-side PHP file upload code for HTML5 File Drag & Drop demonstration
Featured on SitePoint.com
Developed by Craig Buckler (@craigbuckler) of OptimalWorks.net
*/

/*Changes made to account for DiscovART website. No ajax currently 11.17.13*/
$fn = (isset($_SERVER['HTTP_X_FILENAME']) ? $_SERVER['HTTP_X_FILENAME'] : false);

	$files = $_FILES['fileselect'];

	foreach ($files['error'] as $id => $err) {
		if ($err == UPLOAD_ERR_OK) {
			$fn = $files['name'][$id];
			move_uploaded_file(
				$files['tmp_name'][$id],
				'user_art/' . $fn
			);
			//echo "<p>File $fn uploaded.</p>";
		}

	}
	
	if (array_key_exists('submit', $_POST)){
	
		$width= $_POST['l'];
		$length= $_POST['w'];
		$material= $_POST['primematerial'];
		$title= $_POST['title'];
		$category= $_POST['category'];
		$color= $_POST['primecolor'];
		
		uploadArtwork($width, $length, $color, $material, $category, $title, $fn);
		header( 'Location: http://localhost/DiscovART_withDB/examples/myProject/artwork.php' );
	}
?>