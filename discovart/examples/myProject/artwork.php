<?php
include 'lib.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>DiscovART</title>

    <!-- Bootstrap core CSS -->
    <link href="../../dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../dist/css/jumbotron.css" rel="stylesheet">
    <link href="../../dist/css/range.css" rel="stylesheet">
    <link href="../../dist/css/carousel.css" rel="stylesheet">
    <link href="../../dist/css/mymain.css" rel="stylesheet">
    <link href="../../dist/css/customcolorpicker.css" rel="stylesheet">
	<link href="../../dist/css/lightbox.css" rel="stylesheet">
	<link href="../../dist/css/prettyPhoto.css" rel="stylesheet">
	
   

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">DivscovART</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
              <li><a href="index.html">Home</a></li>
              <li class="active"><a href="artwork.php">Art Work</a></li>
              <li><a href="locate.html">Locate</a></li>
			  <li><a href="create.php">Create</a></li>
              <li><a href="about.html">About</a></li>
            </ul>
        </div><!--/.navbar-collapse -->
      </div>
    </div>
   
    <div class="container"><!-- MAIN CONTAINER -->
     
      <!-- Featured Art CONTENT Div -->
	  <div id="content" class="col-md-12">
	  <h2>Featured Art</h2>
		<div class="row">
			<div class="col-md-12">
			<div class="bannerBorder">
				<img src = "images/pageImgs/banner.png" width="60%"><span style="font-weight: bold; font-color: white; text-align: center'"></span>
			</div>
			</div>
		</div>
	  </div>
	  <br>
		
	  <!-- SIDEBAR -->
	  <div id="sideBar" class="col-md-3 myFilterSidebar" style="margin-top: 20px">
			<div class="panel panel-default">
				  <div class="panel-heading">
					<h2 class="panel-title">My Favorites</h2>
				  </div>
				  <div class="panel-body">
				  <div id="favorites" class="widget">
					<p>Drop Favorites Here</p>
					<ul id="faves"></ul>
				  </div><!--end favorites-->           
				  </div>
				</div>       

			<div class="panel panel-default">
				  <div class="panel-heading">
					<h2 class="panel-title">Filter</h2>
				  </div>
				  <div class="panel-body">
						<div class="widget">
						<h4>Dimensions</h4>
				<h5>Width (inches)</h5>
				<form name="form1" method="post" action="">
				  <p>
				<input id="slider" type="range" min="4" max="72" step="2" value="72" onchange="printValue('slider','rangeValue')"/>
				<input id="rangeValue" type="text" size="2"/>
				  </p>
				<h5>Height (inches)</h5>
				  <p>
				<input id="slider1" type="range" min="4" max="72" step="2" value="72" onchange="printValue('slider1','rangeValue1')"/>
				<input id="rangeValue1" type="text" size="2"/>
				  </p><br>
				  <h4>Color</h4>
				<span class="colorpicker">
					<span class="bgbox"></span>
					<span class="hexbox"></span>
					<span class="clear"></span>
					<span class="colorbox">
						<b class="selected" style="background:#000000" title="black"></b>
						<b style="background:#8d8d8d" title="gray"></b>
						<b style="background:#00b300" title="green"></b>
						<b style="background:#0000ff" title="blue"></b>
						<b style="background:#800080" title="purple"></b>
						<b style="background:#e60000" title="red"></b>
						<b style="background:#ffa500" title="orange"></b>
						<b style="background:#a5682a" title="brown"></b>
						<b style="background:#ffff00" title="yellow"></b>
						<b style="background:#fff" title="white"></b>
						<b style="background:transparent; border: 1px solid;" title="none"></b>
					</span>
				</span>
				<br><br>
				<h4>Material</h4>
					<input type="checkbox" id="woodBox" name="material" value="wood"> Wood<br>
					<input type="checkbox" id="oilBox" name="material" value="oil"> Oil<br>
					<input type="checkbox" id="charcoalBox" name="material" value="charcoal"> Charcoal<br>
					<input type="checkbox" id="pencilBox" name="material" value="pencil"> Pencil<br>
					<input type="checkbox" id="paintBox" name="material" value="paint"> Paint<br>
					<input type="checkbox" id="clayBox" name="material" value="clay"> Clay<br>
					<input type="checkbox" id="glassBox" name="material" value="glass"> Glass<br>
						  </form><br>
					  </div><!--end widgets-->       
						  </div>
				</div>       
	 
		</div>           

      <!-- GALLERY content -->
	  <div id="content" class="col-md-8">
		<h2>Gallery</h2>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
              <li><a href="#paintings" data-toggle="tab">Paintings</a></li>
              <li><a href="#drawings" data-toggle="tab">Drawings</a></li>
              <li><a href="#handCrafted" data-toggle="tab">Hand Crafted</a></li>
              <li><a href="#digital" data-toggle="tab">Digital</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active" id="paintings">
              <br>
                    <div class="row">
						<!-- populate with paintings -->
						<?php echo createPaintings(); ?> 						
                    </div> 
              </div>
              <div class="tab-pane" id="drawings">
              <br>
                    <div class="row">
						<!-- populate with drawings -->
						<?php echo createDrawings(); ?> 
                    </div>  
              </div>
              <div class="tab-pane" id="handCrafted">
              <br>
                    <div class="row">
						<!-- populate with handcrafted -->
                        <?php echo createHandcrafted(); ?>    
                    </div>  
              </div>
              <div class="tab-pane" id="digital">
              <br>
                    <div class="row">
						<!-- populate with digital -->
						<?php echo createDigital(); ?>
                    </div>
                </div>
            </div>
            <div class="row"><!-- BROWSE MORE OPTION -->
				<div class="col-md-9">
					<p style="padding-right: 20px"><a href="#">Browse more &raquo;</a></p>
				</div>
			</div>
			
			<!-- button for UPLOAD form -->
            <button type="button" id="uploadBttn">Upload Artwork</button>
            <div id="imageUploader">
			
				<form action="upload.php" method="post" enctype="multipart/form-data">
				<input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="500000">
                    <table>
                        <tr>
                            <td><label>Title:</label></td><td><input id="title" type="text" name="title"></td>
                        </tr>
                        <tr>
                            <td><label>Primary color :</label></td>
                            <td>
                                <select name="primecolor" id="primecolor">
                                    <option value="" selected>--Choose one--</option>
                                    <option value="black">black</option>
                                    <option value="gray">gray</option>
                                    <option value="green"><span style="color:black;font-weight:bold;background-color:green">green</span></option>
                                    <option value="blue"><span style="color:black;font-weight:bold;background-color:blue">blue</span></option>
                                    <option value="purple"><span style="color:black;font-weight:bold;background-color:purple">purple</span></option>
                                    <option value="red"><span style="color:black;font-weight:bold;background-color:red">red</span></option>
                                    <option value="orange"><span style="color:black;font-weight:bold;background-color:orange">orange</span></option>
                                    <option value="brown"><span style="color:black;font-weight:bold;background-color:brown">brown</span></option>
                                    <option value="yellow"><span style="color:black;font-weight:bold;background-color:yellow">yellow</span></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Primary material: </label></td>
                            <td>
                                <select name="primematerial" id="primematerial">
                                    <option value="" selected>--Choose one--</option>
                                    <option value="oil">oil</option>
                                    <option value="wood">wood</option>
                                    <option value="charcoal">charcoal</option>
                                    <option value="pencil">pencil</option>
                                    <option value="paint">paint</option>
                                    <option value="clay">clay</option>
                                    <option value="glass">glass</option>
                                </select>
                            </td>
                        </tr>
						<tr>
							<td><label>Category:</label></td>
                            <td>
                                <select name="category" id="category">
                                    <option value="" selected>--Choose one--</option>
                                    <option value="painting">painting</option>
                                    <option value="drawing">drawing</option>
                                    <option value="digital">digital</option>
                                    <option value="handcrafted">handcrafted</option>
                                </select>
                            </td>						
						</tr>
                        <tr>
                            <td><label>Length (Max 72 in.):</label></td><td><input id="l" size="3" type="text" name="l"></td>
                        </tr>
                        <tr>
                            <td><label>Width (Max 72 in.):</label></td><td><input id="w" size="3" type="text" name="w"></td>
                        </tr>

                    </table>
                    <br>

				<div>
					<label for="fileselect">Files to upload:</label>
					<input type="file" id="fileselect" name="fileselect[]" multiple>
				</div>


				<div id="submitbutton">
					<input type="submit" name="submit" value="Upload">
				</div>
			</form>
			
            </div>
     </div><!-- end of CONTENT Div -->

	 <footer class="footerRed">
			<div style="margin: auto;">
				<p class="text-center">&copy;&nbsp;DiscovART 2013</p>
			</div>
      </footer>
	  
    </div> <!-- eof MAIN CONTAINER -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../dist/js/customColorPicker.js"></script>
    <script src="../../dist/js/jquery.ndd.js"></script>
    <script src="../../dist/js/dragdrop.js"></script>
	<script src="../../dist/js/jquery.prettyPhoto.js"></script>
  <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../dist/js/customColorPicker.js"></script>
    <script src="../../dist/js/jquery.ndd.js"></script>
    <script src="../../dist/js/dragdrop.js"></script>
	<script src="../../dist/js/jquery.prettyPhoto.js"></script>
    <script>

            function printValue(sliderID, textbox) {
                var x = document.getElementById(textbox);
                var y = document.getElementById(sliderID);
                x.value = y.value;
            }

            window.onload = function() {
                printValue('slider', 'rangeValue');
                printValue('slider1', 'rangeValue1');
            }

            var ulButton = document.getElementById("uploadBttn");
            var formDiv = document.getElementById("imageUploader");

            ulButton.onclick = function(){
                var isHidden = formDiv.style.display == 'none';
                formDiv.style.display = isHidden ? 'block': 'none';
            }

        </script>
    <script>
  //Digital Init()
  <?php echo initDigital(); ?>
  <?php echo initPaintings(); ?>
  <?php echo initDrawings(); ?>
  <?php echo initHandcrafted(); ?>
	</script>
	<script>
		var theWidth = 72;
		var theHeight = 72;
		var color = ["black","gray","green","blue","purple","red","orange","brown","yellow","white"];
		
        $(document).ready(function () {

            var form = document.getElementById("imageUploader");
            form.style.display = "none";

            $("#slider").change(function() {
                theWidth = $("#rangeValue").val();
                filter(color);
            });

            $("#slider1").change(function() {
                theHeight = $("#rangeValue1").val();
                filter(color);
            });   

            $("#woodBox").change(function(){
				filter(color);
			});
            
			$("#pencilBox").change(function(){
				filter(color);
			});
            
			$("#clayBox").change(function(){
				filter(color);
			});
            
			$("#oilBox").change(function(){
				filter(color);
			});
            
			$("#charcoalBox").change(function(){
				filter(color);
			});
			
            $("#paintBox").change(function(){
				filter(color);
			});
            
			$("#glassBox").change(function(){
				filter(color);
			});
			
		});

            function filter(colorArray){
				var currentWidth = theWidth;
				var currentHeight = theHeight;
				var pencilBoxState = $("#pencilBox").prop('checked') ? true:false;
				var woodBoxState = $("#woodBox").prop('checked') ? true:false;
				var clayBoxState = $("#clayBox").prop('checked') ? true:false;
				var charcoalBoxState = $("#charcoalBox").prop('checked') ? true:false;
				var paintBoxState = $("#paintBox").prop('checked') ? true:false;
				var glassBoxState = $("#glassBox").prop('checked') ? true:false;
				var oilBoxState = $("#oilBox").prop('checked') ? true: false;
				filterItems(currentWidth, currentHeight, colorArray, woodBoxState, pencilBoxState, clayBoxState, charcoalBoxState, paintBoxState, glassBoxState, oilBoxState);
            }
		
				//Custom Color picker	
			function OnCustomColorChanged(selectedColor, selectedColorTitle, colorPickerIndex) { 
				//alert("Inside onCustomColorChanged method");
				//here we use only one of the passed in parameters: selectedColorTitle 
				var defColorArr = ["black","gray","green","blue","purple","red","orange","brown","yellow","white"];
					
				var colorArr = [selectedColorTitle]; 
					if(selectedColorTitle == 'none')
						color = defColorArr;
					else
						color = colorArr;
					
				filter(color);
			}		
       
			function filterItems(widthCriteria, lengthCriteria, filterColors, isWoodChecked, isPencilChecked, isClayChecked, isCharcoalChecked, isPaintChecked, isGlassChecked, isOilChecked)
			{
				var materials = [];

				if (isWoodChecked == true) materials.push("wood");
				if (isPencilChecked == true) materials.push("pencil");
				if (isClayChecked == true) materials.push("clay");
				if (isCharcoalChecked == true) materials.push("charcoal");
				if (isPaintChecked == true) materials.push("paint");
				if (isGlassChecked == true) materials.push("glass");
				if (isOilChecked == true) materials.push("oil");

				$.each($('.tab-content div.painting'), function(i, item) {
					$item = $(item); 
					itemData = $item.data();
					itemMaterial = itemData.material; //itemMaterial returns an index or -1 if not found.
					materialMatch = materials.indexOf(itemMaterial);

					if(itemData.width <= widthCriteria && itemData.length <= lengthCriteria) 
					{
						if(filterColors.indexOf(itemData.color) != -1){
							if(materialMatch != -1){ // its a match
								$item.animate({opacity: 1});
								itemData.matching = true;
							}
							else if(materials.length === 0){ // its empty -- no materials were checked at all
								$item.animate({opacity: 1});
								itemData.matching = true;
							}
							else{
								$item.animate({opacity: 0.5});
								itemData.matching = false;
							}
						}
						else{
							$item.animate({opacity: 0.5});
							itemData.matching = false;	
						}

					}

					else{
					$item.animate({opacity: 0.5});
					itemData.matching = false;
					}
				});	
			}

			function filterItemsByWidth(widthCriteria)
			{
				$.each($('.tab-content div.painting'), function(i, item) {
					$item = $(item);
					itemData = $item.data();
					if(itemData.width <= widthCriteria)
					{
						$item.animate({opacity: 1});
						itemData.matching = true;
					}
					else
					{
						$item.animate({opacity: 0.5});
						itemData.matching = false;
					}
				});
			}
       
			//Added 12/6/2013
			function filterItemsByLength(lengthCriteria)
			{
				$.each($('.tab-content div.painting'), function(i, item) {
					$item = $(item);
					itemData = $item.data();
					if(itemData.length <= lengthCriteria)
					{
						$item.animate({opacity: 1});
						itemData.matching = true;
					}
					else
					{
						$item.animate({opacity: 0.5});
						itemData.matching = false;
					}
				});
			}
    </script>   
    <script>
      $(function () {
        $('#myTab a:last').tab('show')
      })
    </script>
	<script>
  $(document).ready(function(){
    $("a[rel^='prettyPhoto']").prettyPhoto();
  });	
	</script>
  </body>
</html>