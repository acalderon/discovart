<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>DiscovART</title>

    <!-- Bootstrap core CSS -->
    <link href="../../dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../dist/css/jumbotron.css" rel="stylesheet">
	<link href="../../dist/css/storelocator.css" rel="stylesheet">
	<link href="../../dist/css/range.css" rel="stylesheet">
	<link href="../../dist/css/carousel.css" rel="stylesheet">
	<link href="../../dist/css/mymain.css" rel="stylesheet">
	<link href="../../dist/css/customcolorpicker.css" rel="stylesheet">
	<link href="../../dist/css/progressSteps.css" rel="stylesheet">
	<link href="../../dist/css/newMain.css" rel="stylesheet">


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
	  
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">DivscovART</a>
        </div>
        <div class="navbar-collapse collapse">
			<ul class="nav navbar-nav pull-right">
			  <li class="#"><a href="index.html">Home</a></li>
			  <li class="#"><a href="artwork.php">Art Work</a></li>
			  <li><a href="locate.html">Locate</a></li>
			  <li class="active"><a href="create.php">Create</a></li>
			  <li class="about.html"><a href="#">About</a></li>
			</ul>
        </div><!--/.navbar-collapse -->
      </div>
    </div>
	
    <div class="container">
  <h1>Create Your Own Fun Pic!</h1>
  
 <div class="wizard">
    <a href="create.php"><span class="badge">1</span>Start</a>
    <a href="upload_photo.php" class="current"><span class="badge badge-inverse">2</span>Upload</a>
    <a href="modify.php"><span class="badge">3</span>Modify</a>
    <a href="#"><span class="badge">4</span>Caption</a> 
</div> 
<div id="content">
  <h1>Step 2: Upload a photo</h1>
  <div id="showChoice"></div>
<form id="upload" action="uploadFunPic.php" method="POST" enctype="multipart/form-data">

<fieldset>
<legend>HTML File Upload</legend>

<input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="500000" />

<div>
	<label for="fileselect">Files to upload:</label>
	<input type="file" id="fileselect" name="fileselect[]" multiple />
	<div id="filedrag">or drop files here</div>
</div>


<div id="submitbutton">
	<button type="submit">Upload Files</button>
</div>

</fieldset>

</form>

<div id="progress"></div>

<div id="messages">
<!-- <p>Status Messages</p> -->
</div>
</div>


      <footer class="myfooter">
	  <div style="margin: auto;">
        <p class="text-center">&copy; DiscovART 2013</p>
		</div>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
	<script src="../../dist/js/filedrag.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			var $theChoice = localStorage.getItem("theChoice");
			$theChoice = "<p>For your background, you chose <strong>"+$theChoice+"</strong>.</p>";
			$("#showChoice").html($theChoice);
		});
	</script>
  </body>
</html>
